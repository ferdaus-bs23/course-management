# -*- coding: utf-8 -*-
{
    'name': "course_management",

    'summary': """
        Test purpose only""",

    'description': """
        A course management module in order to test what I have learn and follow the odoo guidelines.
    """,

    'author': "Fazle Rabbi Ferdaus",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/course_management_instructor_view.xml',
        'views/course_managemnet_enrollment_record_view.xml',
        'views/course_management_student_views.xml',
        'views/course_mangement_course_views.xml',
        'views/course_management_menus.xml',
    ],
    'application': True,
}