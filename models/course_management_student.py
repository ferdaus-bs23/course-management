import math

from dateutil.relativedelta import relativedelta

from odoo import fields, models, api

class Student(models.Model):
    _name = "course.management.student"
    _description = "Student model for course management"

    name = fields.Char(string="Name", required=True)
    gender = fields.Selection(
        selection=[('male', 'Male'), ('female', 'Female')]
    )
    date_of_birth = fields.Date(string="Date of birth", default=fields.Date.today(), required=True)
    age = fields.Integer(string='Age', compute="_compute_age")
    email = fields.Char(string='Email')

    @api.depends("date_of_birth")
    def _compute_age(self):
        for record in self:
            days_x = (fields.Date.from_string(fields.Date.today()) - fields.Date.from_string(record.date_of_birth)).days
            years = math.floor(days_x/365)
            record.age = years

