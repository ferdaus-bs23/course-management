import math

from dateutil.relativedelta import relativedelta

from odoo import fields, models, api

class Course(models.Model):
    _name = "course.management.course"
    _description = "Course model for course management"

    name = fields.Char(string="Name", required=True)
    description = fields.Text(string="description")
    date_start = fields.Date(string="Start Date", default=fields.Date.today(),required=True )
    duration = fields.Integer(string="Duration (months)", default=1, required=True)
    end_date = fields.Date(string="End date", compute="_compute_end_date", inverse="_inverse_end_date")
    instructor_id = fields.Many2one(string="Instructor", comodel_name="course.management.instructor")
    apply_deadline = fields.Date(string="Application Deadline")
    student_id = fields.Many2many(comodel_name="course.management.student")
    student_count = fields.Integer(string="Student cnt.", compute="_compute_student_count")
    enrollment_ids = fields.One2many(comodel_name="course.management.enrollment.record", inverse_name="course_id")
    enrollment_count = fields.Integer(compute="_compute_enrollment_count")

    @api.depends("date_start","duration")
    def _compute_end_date(self):
        for record in self:
            record.end_date = fields.Date.from_string(record.date_start) + relativedelta(months=+record.duration)
            # print(fields.Date.from_string(record.date_start) + relativedelta(months=+record.duration))
            # print(fields.Date.from_string(record.date_start))
    def _inverse_end_date(self):
        for record in self:
            days_x = (fields.Date.from_string(record.end_date) - fields.Date.from_string(record.date_start)).days
            months = math.ceil(days_x/30)
            record.duration = months

    @api.depends("student_id")
    def _compute_student_count(self):
        for record in self:
            record.student_count = len(record.student_id.mapped('id'))

    @api.depends("enrollment_ids")
    def _compute_enrollment_count(self):
        for record in self:
            record.enrollment_count = len(record.mapped('enrollment_ids'))
            # print(record.mapped('enrollment_ids'))

