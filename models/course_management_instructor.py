from odoo import fields, models, api

class Instructor(models.Model):
    _name = "course.management.instructor"
    _description = "Instructors model for course management"

    name = fields.Char(string="Name", required=True)
    bio = fields.Char(string="Bio")
    email = fields.Char(string="Email", required=True)
    phone = fields.Char(string="Phone", required=True)

    course_ids = fields.One2many(string="Courses", comodel_name="course.management.course", inverse_name="instructor_id")
    course_count = fields.Integer(string="course cnt.", compute="_compute_course_count")


    @api.depends("course_ids")
    def _compute_course_count(self):
        for record in self:
            record.course_count = len(record.mapped('course_ids'))