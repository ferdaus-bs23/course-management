from odoo import fields, models, api

class EnrollmentRecord(models.Model):
    _name = 'course.management.enrollment.record'
    _description = "Course enrollment records"

    student_id = fields.Many2one(string='Student', comodel_name="course.management.student", required=True)
    course_id = fields.Many2one(string='Course', comodel_name="course.management.course", required=True)
