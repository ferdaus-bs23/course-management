import unittest
from odoo.tests import common


class TestCourseManagement(common.TransactionCase):

    def test_creation_end_date(self):
        test_course = self.env['course.management.course'].create({
            'name': 'TestProjectfromtestcase'
        })
        self.assertEqual(test_course.name, 'TestProject')
